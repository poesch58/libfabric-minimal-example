#pragma once

#include "rdma/fabric.h"
#include <pthread.h>
#include <stdbool.h>
#include <stddef.h>

#define DP_AV_DEF_SIZE 512
#define COMM_FILE_WRITER_TO_READER "./writer_address.bin"
#define DATA_LEN 128

struct fabric_state
{
    struct fi_context *ctx;
    struct fi_info *info;
    struct fid_fabric *fabric;
    struct fid_domain *domain;
    struct fid_ep *signal;
    struct fid_cq *cq_signal;
    struct fid_av *av;

    fi_addr_t comm_partner;
    char send_data[DATA_LEN];
    struct fid_mr *mr;
    uint64_t key;
    uint64_t remote_address;
};

void init_fabric(struct fabric_state *fabric);

static bool is_local_mr_req(struct fabric_state *f)
{
    return (f->info->mode & FI_LOCAL_MR) != 0;
}