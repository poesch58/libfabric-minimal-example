#include "common.h"
#include "rdma/fabric.h"
#include "rdma/fi_cm.h"
#include "rdma/fi_domain.h"
#include "rdma/fi_errno.h"
#include "rdma/fi_rma.h"
#include "sys/stat.h"
#include "unistd.h"
#include <stdio.h>
#include <stdlib.h>

int enter_other_address_from(
    struct fabric_state *fabric_state, char const *filename)
{
    struct stat buffer;
    while (stat(filename, &buffer) != 0)
    {
        printf("Waiting for '%s'\n", filename);
        sleep(5);
    }
    FILE *address_file = fopen(filename, "rb");
    size_t address_len;
    {
        int read_items = fread(&address_len, sizeof(size_t), 1, address_file);
        if (read_items != 1)
        {
            fprintf(
                stderr,
                "Read only %dl items from '%s'\n",
                read_items,
                filename);
            return 1;
        }
    }
    printf("Address len: %zul\n", address_len);

    void *address = malloc(address_len);
    {
        int read_items = fread(address, 1, address_len, address_file);
        if (read_items != address_len)
        {
            fprintf(
                stderr,
                "Read only %dl items from '%s'\n",
                read_items,
                filename);
            return 1;
        }
    }
    fi_addr_t fi_addr;
    fi_av_insert(
        fabric_state->av, address, 1, &fabric_state->comm_partner, 0, NULL);
    free(address);

    {
        int read_items =
            fread(&fabric_state->key, sizeof(uint64_t), 1, address_file);
        if (read_items != 1)
        {
            fprintf(
                stderr,
                "Read only %dl items from '%s'\n",
                read_items,
                filename);
            return 1;
        }
        read_items = fread(
            &fabric_state->remote_address, sizeof(void *), 1, address_file);
        if (read_items != 1)
        {
            fprintf(
                stderr,
                "Read only %dl items from '%s'\n",
                read_items,
                filename);
            return 1;
        }
    }
    fclose(address_file);
    return 0;
}

int read_from_remote(struct fabric_state *fabric_state)
{
    // register dest buffer
    fi_mr_reg(
        fabric_state->domain,
        fabric_state->send_data,
        DATA_LEN,
        FI_READ,
        0,
        0,
        0,
        &fabric_state->mr,
        NULL);
    void *memory_descriptor = NULL;
    if (is_local_mr_req(fabric_state))
    {
        memory_descriptor = fi_mr_desc(fabric_state->mr);
    }

    size_t rc;
    do
    {
        rc = fi_read(
            fabric_state->signal,
            fabric_state->send_data,
            DATA_LEN,
            memory_descriptor,
            fabric_state->comm_partner,
            fabric_state->remote_address,
            fabric_state->key,
            NULL);
    } while (rc == -EAGAIN);
    if (rc != 0)
    {
        fprintf(stderr, "fi_read failed with code %zu.\n", rc);
        return (rc);
    }

    struct fi_cq_data_entry CQEntry = {0};
    rc = fi_cq_sread(fabric_state->cq_signal, &CQEntry, 1, NULL, -1);
    if (rc < 1)
    {
        fprintf(stderr, "Received no completion event for remote read\n");
        return 1;
    }

    return 0;
}

int main()
{
    struct fabric_state fabric_state;
    init_fabric(&fabric_state);
    if (enter_other_address_from(&fabric_state, COMM_FILE_WRITER_TO_READER) !=
        0)
    {
        fprintf(stderr, "enter_other_address_from failed");
        return 1;
    }
    if (read_from_remote(&fabric_state) != 0)
    {
        fprintf(stderr, "read_from_remote failed");
        return 1;
    }
    printf("Received remote message:\n%s\n", fabric_state.send_data);
}