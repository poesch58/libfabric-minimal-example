#include "common.h"
#include "rdma/fi_cm.h"
#include "rdma/fi_domain.h"
#include "rdma/fi_errno.h"
#include "unistd.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#define SEND_TEXT                                                              \
    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam "       \
    "nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, "  \
    "sed diam voluptua. At vero eos et accusam et justo duo dolores et ea "    \
    "rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem "     \
    "ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing " \
    "elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna " \
    "aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo "   \
    "dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus " \
    "est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur "  \
    "sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et "   \
    "dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam "   \
    "et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea "     \
    "takimata sanctus est Lorem ipsum dolor sit amet."

int publish_my_address_to(
    struct fabric_state *fabric_state, char const *filename)
{
    fi_mr_reg(
        fabric_state->domain,
        fabric_state->send_data,
        DATA_LEN,
        FI_WRITE | FI_REMOTE_READ,
        0,
        0,
        0,
        &fabric_state->mr,
        NULL);
    fabric_state->key = fi_mr_key(fabric_state->mr);

    char address[DP_AV_DEF_SIZE];
    size_t address_len = DP_AV_DEF_SIZE;
    int status = fi_getname((fid_t)fabric_state->signal, address, &address_len);
    if (status != FI_SUCCESS)
    {
        fprintf(stderr, "fi_getname failed: %s\n", fi_strerror(status));
        return 1;
    }
    FILE *address_file = fopen(filename, "wb");
    fwrite(&address_len, sizeof(size_t), 1, address_file);
    fwrite(address, 1, address_len, address_file);
    fwrite(&fabric_state->key, sizeof(uint64_t), 1, address_file);
    size_t pointer_addr = (size_t)&fabric_state->send_data;
    fwrite(&pointer_addr, sizeof(void *), 1, address_file);
    fclose(address_file);
    return 0;
}

int main()
{
    struct fabric_state fabric_state;
    {
        char const *send_text = SEND_TEXT;
        for (size_t i = 0; i < DATA_LEN; ++i)
        {
            fabric_state.send_data[i] = send_text[i];
        }
        fabric_state.send_data[DATA_LEN - 1] = 0;
    }
    init_fabric(&fabric_state);
    if (publish_my_address_to(&fabric_state, COMM_FILE_WRITER_TO_READER) != 0)
    {
        fprintf(stderr, "publish_my_address_to failed");
        return 1;
    }
    while (true)
    {
        sleep(10);
        // printf("Writer still running\n");
    }
}